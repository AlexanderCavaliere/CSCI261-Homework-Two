import java.util.Scanner;

public class Majority {

    public static void main( String[] args ) {
        Scanner sc = new Scanner( System.in );
        int n = sc.nextInt();
        int[] values = new int[n];
        for( int i = 0; i < n; ++i ) {
            values[i] = sc.nextInt();
        }

	int alpha = Select( values, n/2 );
	int beta = Select( values, n/3 );
    }

    public static int[] sort( int[] A )
    {
	int temp = 0;
	int tempJ = 0;
	for( int i = 0; i < A.length; i++ )
	    {
		tempJ = i;
		for( int j = i + 1; j < A.length; j++ )
		    {
			if( A[j] <= A[tempJ] )
			    {	
				tempJ = j;
			    }
		    }
		temp  = A[i];
		A[i] = A[tempJ];
		A[tempJ] = temp;
	    }
	return A;
    }
    
    public static int Select( int[] A , int k )
    {
	if( A.length <= 5 )
	    {
		sort( A );
		return A[k];
	    }
	int medSize = (A.length / 5 );
	int leftover = A.length % 5;
	
	if( leftover != 0 )
	    {
		medSize++;
	    }
	
	int[] medians = new int[medSize];
	int[] part = new int[5];
	int[] remains = new int[leftover];
	int j = 0;
	for( int i = 0; i < A.length - leftover; ++i )
	    {
		part[i%5] = A[i];
		if( i % 5 == 0  && i != 0)
		    {
			medians[j] = Select( part, part.length / 2 );
			++j;
		    }
	    }
	if( leftover != 0 )
	    {
		for( int i = A.length - 1; i > A.length - leftover; --i )
		    {
			remains[i%leftover] = A[i];
		    }
		
		medians[++j] = Select( remains, remains.length /2 );
	    }
	int medianB = Select( medians, medians.length / 2 );

	int[] ARearranged = new int[A.length];
	int j1 = 0;
	int j2 = A.length - 1;
	for( int i = 0; i < A.length; i++ )
	    {
		if( A[i] < medianB )
		    {
			ARearranged[j1] = A[i];
			++j1;
		    }
		else if( A[i] > medianB )
		    {
			ARearranged[j2] = A[i];
			--j2;
		    }
	    }
	for( int i = j1; i <= j2; ++i )
	    {
		ARearranged[i] = medianB;
	    }

	if( k < j1 )
	    {
		int[] ARearrangedLeft = new int[j1];
		for(int i = 0; i < ARearrangedLeft.length; ++i )
		    {
			ARearrangedLeft[i] = ARearranged[i];
		    }
		return Select( ARearrangedLeft, k );
	    }
	if( j1 <= k && k <= j2 )
	    {
		if( j2 - j1 > k )
		    {
			System.out.println( "YES" );
		    }
		else
		    System.out.println( "NO" );
		return medianB;
	    }
	if( k > j2 )
	    {
		int[] ARearrangedRight = new int[A.length - j2];
		for( int i = j2 + 1; i < A.length; i++ )
		    {
			ARearrangedRight[i-j2] = ARearranged[i];
		    }
		return Select( ARearrangedRight, k - j1 );
	    }
	return -99;
    }
			
}
