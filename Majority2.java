import java.util.Scanner;

public class Majority2 {

    public static void main( String[] args ) {
        Scanner sc = new Scanner( System.in );
        int n = sc.nextInt();
        int[] values = new int[n];
        for( int i = 0; i < n; ++i ) {
            values[i] = sc.nextInt();
        }

	int alphaCount = 0;
	int alpha = select( values, ( ( n + 1) / 2 ) - 1, 0, values.length - 1 );
	for( int i = 0; i < n; i++ )
	    {
		if( values[i] == alpha )
		    alphaCount++;
	    }
	       
	if( alphaCount > n / 2 )
	    {
		System.out.println( "YES" );
	    }
	else
	    {
		System.out.println( "NO" );
	    }

	int betaCount = 0;
	int beta = select( values, n/3, 0, values.length - 1 );

	for( int i = 0; i < n; i++ )
	    {
		if( values[i] == beta )
		    betaCount++;
	    }

	if( betaCount > n / 3 )
	    System.out.println( "YES" );
	else
	    System.out.println( "NO" );
	
	System.out.println( "We've made it; no exceptions!" );
    }

    public static void sort( int[] A, int left, int right )
    {
	int temp = 0;
	int tempJ = 0;
	for( int i = 0; i < A.length; i++ )
	    {
		tempJ = i;
		for( int j = i + 1; j < A.length; j++ )
		    {
			if( A[j] <= A[tempJ] )
			    {	
				tempJ = j;
			    }
		    }
		temp  = A[i];
		A[i] = A[tempJ];
		A[tempJ] = temp;
	    }
    }

    public static int select( int[] A, int k, int left, int right )
    {
	if( right - left <= 5 )
	    {
		sort( A, left, right );
		return A[k];
	    }
	
	int mediansLength = ( ( right + 1 ) - left) / 5;
	int extraLength = ( ( right + 1 ) - left) % 5;
	if( extraLength != 0 )
	    mediansLength++;

	int[] medians = new int[mediansLength];
	int medianPosition = 0;
	
	for( int i = left; i <= right - 4 - extraLength; i += 4 )
	    {
		//System.out.printf( "Left: %d, Right: %d\n", i, i+ 4  );
		medians[medianPosition] = select( A, 2, i, i + 4 );
		if( i == 0 ) i++;
		medianPosition++;
	    }
	if( extraLength != 0 )
	    medians[medianPosition] = select( A, ( (extraLength + 1) / 2) - 1, right - extraLength, right );

	int medianB = select( medians, ( ( mediansLength + 1 ) / 2) - 1, 0, mediansLength - 1);

	int tempLeft = left;
	for( int i = left; i <= right; i++ )
	    {
		if( A[i] < medianB )
		    {
			int temp = A[i];
			A[i] = A[tempLeft];
			A[tempLeft] = A[i];
			tempLeft++;
		    }
	    }

	if( k < tempLeft )
	    return select( A, k, 0, tempLeft - 1 );

	if( k == tempLeft )
	    return medianB;

	if( k > tempLeft )
	    return select( A, k - tempLeft, tempLeft + 1, right );

	return -1;
    }
	
	
}
