/**
 * MaxRectange.java
 * @authors Alexander R. Cavaliere <arc6393@rit.edu>
 *          Jon Schenk <jds8899@rit.edu>
 */

import java.util.Scanner;

public class MaxRectangle
{
    public static void main( String[] args )
    {
	Scanner scanner = new Scanner( System.in );
	int n = scanner.nextInt();

	Bar[] bars = new Bar[(n/2) - 1];
	int barCount = 0;
	int x1 = Math.abs( scanner.nextInt() );
	int y1 = scanner.nextInt();

	
	for( int line = 1; line < n/2; line++ )
	    {
		int temp = scanner.nextInt();	
		
		temp = scanner.nextInt();
		int x2 = Math.abs( scanner.nextInt() );
	
		int y2 = scanner.nextInt();
	        
		bars[barCount] = new Bar( y2, x2 - x1 );
	        barCount++;
		x1 = x2;
		y1 = y2;
	    }
	
	
	BarStack left = new BarStack( bars.length );
	BarStack right = new BarStack( bars.length );

	Bar[] internalLeft = new Bar[bars.length];
	Bar[] internalRight = new Bar[bars.length];

	// Perform left operation
	for( int position = 0; position < bars.length; position++ )
	    {
		long currentW = bars[position].getWidth();
		while( !left.isEmpty() && bars[position].getHeight() <= left.peek().getHeight() )
		    {
			currentW += left.pop().getWidth();
		    }	
		left.push( new Bar( bars[position].getHeight(), currentW ) );
		internalLeft[position] = new Bar( bars[position].getHeight(), currentW );
	    }


	for( int position = bars.length - 1; position >= 0; position-- )
	    {
		long currentW = bars[position].getWidth();
		while( !right.isEmpty() && bars[position].getHeight() <= right.peek().getHeight() )
		    {
			currentW += right.pop().getWidth();
		    }
		right.push( new Bar( bars[position].getHeight(), currentW ) );
		internalRight[position] = new Bar( bars[position].getHeight(), currentW );
	    }
	
	// Find Area

	long max = -1;

	ArrayList<Long> targetDerivedW = new ArrayList<Long>();
	
	for( int position = 0; position < internalLeft.length; position++ )
	    {
		max = Math.max( max, internalLeft[position].getHeight() * ( internalLeft[position].getWidth() + internalRight[position].getWidth() - bars[position].getWidth() ) ); 
	    }
				    
	System.out.println( max );
    }    
}

class Bar
{
    // Private Class State
    private long height;
    private long width;
    
    /**
     * Bar
     *
     * Public constructor
     * @param height
     * The max height of the bar
     * @param width
     * The max width of the bar
     */
    public Bar( long height, long width )
    {
	this.height = height;
	this.width = width;
    }
    
    // Getters and Setters
    public long getHeight()
    {
	return this.height;
    }
    
    public long getWidth()
    {
	return this.width;
    }
    
    public void setHeight( long height )
    {
	this.height = height;
    }
    
    public void setWidth( long width )
    {
	this.width = width;
    }		    
}

class BarStack
{
    // Private Class State
    private Bar[] stack;
    private int top;
    private int stackSize; // Total Size of the array
    private int stackCount; // Number of elements in the stack
    
    public BarStack( int stackSize )
    {
	this.stackSize = stackSize;
	stack = new Bar[this.stackSize];
	top = -1;
	stackCount = 0;
    }
    
    public Bar pop()
    {
	Bar bar = stack[top];
	top--;
	stackCount--;
	return bar;
    }
    
    public Bar peek()
    {
	return stack[top];
    }
    
    public void push( Bar bar )
    {
	stackCount++;
	top++;
	stack[stackCount-1] = bar;
    }
    
    public boolean isEmpty()
    {
	if( stackCount == 0 )
	    return true;
	return false;
	
    }
}

